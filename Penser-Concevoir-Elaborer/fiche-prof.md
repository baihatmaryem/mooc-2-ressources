# Fiche de prof
- Thématique : Les logiciels (traitement de texte).
- Notions liées :  saisie, insertion d’objets.
- Résumé de l’activité : Activité débranchée en deux parties. Traitement de texte insertion des images.
- Objectifs :
Exploiter les fonctionnalités d’un texteur pour produire un document mis en forme, contenant des tableaux et éventuellement des objets graphiques 
- Auteur: BAIHAT Maryem
- Durée de l’activité : 2h.
- Forme de participation : en binôme, collective.
- Matériel nécessaire : Papier, stylo, ordinateur.
- Préparation : Aucune
- Fiche Elève :          
                                    
Reproduisez la mise en forme du CV  (Enregistrez régulièrement votre travail)

1.  Créez un nouveau document et enregistrez-le immédiatement 
2.  Saisissez le texte au kilomètre dans un nouveau document 
3.  Corrigez l’orthographe 
4.  Insérez les 2 caractères spéciaux 
5.  Ajoutez les 22 tabulations 
6.  Faites les modifications de polices (gras, italique, police : Times 12 ou16) 
7.  Modifiez le retrait du premier paragraphe (8 cm) du paragraphe formations (1.25 cm) et centrez l’objectif 
8.  Créez les 2 listes à puces 
9.  Ajoutez les bordures et trames de fond 
