# Fiche  d'activité : 
Une activité pour commencer à repérer les fonctions les plus simples de mise en page : gras, italique, centré, taille de la police, choix de la police, justifié, mettre en couleur… L’activité est équivalente à la numéro 4 (mettre en page un extrait de Dico dingo). Celle-ci montre un texte plus court, mais avec une mise en page à faire plus compliquée.

## Etape 0:
    Formuler des binôme Différent, distribuer les roles entre les binomes.

    
## Etape 1 :
    Un exercice de mise en page : un texte à mettre en forme selon un modèle, avec des consignes. Travail lié à l’exploitation du conte . 
