# Activite 

Votre établissement produit chaque fin d’année un nouveau numéro de la revue d’établissement.
Vous faites partie de l’une des équipes de production et de rédaction de la revue. Cette équipe est chargée de réaliser des productions touchant les rubriques suivantes : Carte d’identité, thème de l’année.
Carte d’identité : 
## le travail à réaliser
      
     ° Une page sur l’établissement contenant :
     ° Un titre.
     ° Une photo récente de l’établissement (à scanner ou à l’aide d’un appareil photo numérique).
     ° Un petit historique sur l’établissement.

## Choix logiciels : logiciel de Traitement de texte.
## Fiche descriptive du document à réaliser :
    - Photos à insérer dans le document : 
          Cherchez les photos sur internet ou scannez des photos à partir d’encyclopédies appartenant à la bibliothèque de l’établissement.
    - Citez les références des photos insérées.
